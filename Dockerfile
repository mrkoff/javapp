FROM openjdk:11

COPY . /usr/src/myapp
WORKDIR /usr/src/myapp/
EXPOSE 8080
RUN ./mvnw clean package
CMD ["./mvnw","spring-boot:run"]
